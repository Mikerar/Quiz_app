/**
 * Created by mikhailSapozhnikov on 2/22/21.
 */

@IsTest
public with sharing class EnwayQuizControllerTest {
  @testSetup
  static void setup() {
    Test__c test = new Test__c();
    insert test;

    List<Question__c> relQuestions = new List<Question__c>();
    for (Integer i = 0; i < 5; i++) {
      Question__c question = new Question__c(Test__c = test.Id);
      relQuestions.add(question);
    }
    insert relQuestions;

    List<Answer__c> relAnswers = new List<Answer__c>();
    for (Question__c question : relQuestions) {
      for (Integer i = 0; i < 5; i++) {
        Answer__c ans = new Answer__c(Question__c = question.Id);
        relAnswers.add(ans);
      }
    }
    insert relAnswers;

    User_Test_Answer__c uta = new User_Test_Answer__c(
      Test__c = test.Id,
      User_Name__c = 'Test'
    );
    insert uta;

    User_Question_Answer__c uqa = new User_Question_Answer__c(
      User_Test_Answer__c = uta.Id,
      Check_Question_Correct__c = true
    );
    insert uqa;
  }

  @IsTest
  static void TestGetAllTests() {
    List<Test__c> tests = EnwayQuizController.getAllTests();
    System.assertEquals(tests.size(), 1);
  }

  @IsTest
  static void TestGetRelatedQuestion() {
    List<Test__c> tests = [SELECT Id FROM Test__c LIMIT 1];
    Test__c test = (tests.size() == 1) ? tests.get(0) : null;
    List<Question__c> relQuestions = EnwayQuizController.getRelatedQuestion(
      test.Id
    );
    System.assertEquals(relQuestions.size(), 5);
  }

  @IsTest
  static void TestGetRelatedAnswers() {
    List<Question__c> questions = [SELECT Id FROM Question__c LIMIT 1];
    Question__c question = (questions.size() == 1) ? questions.get(0) : null;
    List<Answer__c> relAnswers = EnwayQuizController.getRelatedAnswers(
      question.Id
    );
    System.assertEquals(relAnswers.size(), 5);
  }

  @IsTest
  static void TestGetRelatedResult() {
    List<Test__c> tests = [SELECT Id FROM Test__c LIMIT 1];
    Test__c test = (tests.size() == 1) ? tests.get(0) : null;
    List<EnwayQuizController.resultClass> resultClassesList = (List<EnwayQuizController.resultClass>) JSON.deserialize(
      EnwayQuizController.getRelatedResult(test.id),
      List<EnwayQuizController.resultClass>.class
    );
    Decimal result;
    for (EnwayQuizController.resultClass resCls : resultClassesList) {
      result = resCls.Result;
      break;
    }
    System.assertEquals(result, 20);
  }

  @IsTest
  static void TestSaveUserTest() {
    List<Test__c> tests = [SELECT Id FROM Test__c LIMIT 1];
    Test__c test = (tests.size() == 1) ? tests.get(0) : null;

    User_Test_Answer__c uta = new User_Test_Answer__c(
      Test__c = test.Id,
      User_Name__c = 'Test'
    );

    List<User_Question_Answer__c> uqa = new List<User_Question_Answer__c>();
    Map<String, List<User_Answers__c>> ua = new Map<String, List<User_Answers__c>>();
    for (Question__c q : [
      SELECT Id
      FROM Question__c
      WHERE Test__c = :test.Id
    ]) {
      User_Question_Answer__c uqa_iterate = new User_Question_Answer__c(
        Question__c = q.Id
      );
      uqa.add(uqa_iterate);
      User_Answers__c ua_iterate = new User_Answers__c(
        User_Question_Answer__c = q.Id
      );
      List<User_Answers__c> ua_list = new List<User_Answers__c>();
      ua_list.add(ua_iterate);
      ua.put(q.Id, ua_list);
    }

    Id id = EnwayQuizController.saveUserTest(
      JSON.serialize(uta),
      JSON.serialize(uqa),
      JSON.serialize(ua)
    );
    List<User_Test_Answer__c> userTestAnswers = [
      SELECT Id, User_Result__c
      FROM User_Test_Answer__c
      WHERE Id = :id
    ];

    System.assert(userTestAnswers.size() == 1);
    System.assertEquals(userTestAnswers[0].User_Result__c, 100);
  }

  @IsTest
  static void testGetResult() {
    List<User_Test_Answer__c> userTestAnswers = [
      SELECT Id
      FROM User_Test_Answer__c
      LIMIT 1
    ];
    User_Test_Answer__c userTestAnswer = (userTestAnswers.size() == 1)
      ? userTestAnswers.get(0)
      : null;
    String str = EnwayQuizController.getResult(userTestAnswer.Id);
    List<EnwayQuizController.userModalResult> modalResultClassesList = (List<EnwayQuizController.userModalResult>) JSON.deserialize(
      EnwayQuizController.getResult(userTestAnswer.Id),
      List<EnwayQuizController.userModalResult>.class
    );
    Decimal result;
    Decimal questionAmount;
    Decimal correctQuestionAmount;
    for (
      EnwayQuizController.userModalResult modalResult : modalResultClassesList
    ) {
      result = modalResult.result;
      questionAmount = modalResult.questionAmount;
      correctQuestionAmount = modalResult.correctQuestionAmount;
      break;
    }

    System.assertEquals(modalResultClassesList.size(), 1);
    System.assertEquals(questionAmount, 5);
    System.assertEquals(correctQuestionAmount, 1);
    System.assertEquals(result, 20);
  }
}
