public without sharing class EnwayQuizController {
  @AuraEnabled(Cacheable=true)
  public static List<Test__c> getAllTests() {
    return [SELECT Id, Name, Question_amount__c FROM Test__c];
  }

  @AuraEnabled(Cacheable=true)
  public static List<Question__c> getRelatedQuestion(String testId) {
    return [
      SELECT Id, Question_Text__c
      FROM Question__c
      WHERE Test__c = :testId
    ];
  }

  @AuraEnabled(Cacheable=true)
  public static List<Answer__c> getRelatedAnswers(String questionId) {
    return [
      SELECT Id, Answer_Long__c, isCorrect__c
      FROM Answer__c
      WHERE Question__c = :questionId
    ];
  }

  @AuraEnabled(Cacheable=true)
  public static String getResult(String userTestAnswerId) {
    List<userModalResult> umd_list = new List<EnwayQuizController.userModalResult>();
    List<User_Test_Answer__c> userTestAnswers = [
      SELECT
        User_Result__c,
        User_Correct_Question_Amount__c,
        Test__r.Question_amount__c,
        Test__r.Name
      FROM User_Test_Answer__c
      WHERE Id = :userTestAnswerId
      LIMIT 1
    ];
    for (User_Test_Answer__c uta : userTestAnswers) {
      userModalResult resClass = new userModalResult();
      resClass.id = uta.Id;
      resClass.testName = uta.Test__r.Name;
      resClass.result = uta.User_Result__c;
      resClass.questionAmount = uta.Test__r.Question_amount__c;
      resClass.correctQuestionAmount = uta.User_Correct_Question_Amount__c;
      umd_list.add(resClass);
    }

    return JSON.serialize(umd_list);
  }

  public class userModalResult {
    public Id id { get; set; }
    public Decimal result { get; set; }
    public String testName { get; set; }
    public Decimal questionAmount { get; set; }
    public Decimal correctQuestionAmount { get; set; }
  }

  @AuraEnabled(cacheable=true)
  public static String getRelatedResult(String testId) {
    List<resultClass> resultClasses = new List<EnwayQuizController.resultClass>();
    List<User_Test_Answer__c> userTestAnswers = [
      SELECT Id, User_Name__c, User_Result__c
      FROM User_Test_Answer__c
      WHERE Test__c = :testId
    ];
    for (User_Test_Answer__c uta : userTestAnswers) {
      resultClass resClass = new resultClass();
      resClass.id = uta.Id;
      resClass.Name = uta.User_Name__c;
      resClass.Result = uta.User_Result__c;
      resultClasses.add(resClass);
    }
    return JSON.serialize(resultClasses);
  }

  public class resultClass {
    public Id id { get; set; }
    public String Name { get; set; }
    public Decimal Result { get; set; }
  }

  //    this.userTestAnswer, this.userQuestionAnswers, this.userAnswers
  @AuraEnabled
  public static Id saveUserTest(
    String userTestAnswer,
    String userQuestionAnswers,
    String userAnswers
  ) {
    User_Test_Answer__c uta = (User_Test_Answer__c) JSON.deserialize(
      userTestAnswer,
      User_Test_Answer__c.class
    );
    insert uta;

    List<User_Question_Answer__c> uqa = (List<User_Question_Answer__c>) JSON.deserialize(
      userQuestionAnswers,
      List<User_Question_Answer__c>.class
    );
    for (User_Question_Answer__c uqa_iteration : uqa) {
      uqa_iteration.User_Test_Answer__c = uta.Id;
    }
    insert uqa;

    Map<String, List<User_Answers__c>> ua = (Map<String, List<User_Answers__c>>) JSON.deserialize(
      userAnswers,
      Map<String, List<User_Answers__c>>.class
    );

    if (!ua.values().isEmpty()) {
      List<User_Answers__c> ua_list = new List<User_Answers__c>();
      for (User_Question_Answer__c uqa_iteration : uqa) {
        if (ua.containsKey(uqa_iteration.Question__c)) {
          for (
            User_Answers__c ua_iteration : ua?.get(uqa_iteration.Question__c)
          ) {
            ua_iteration.User_Question_Answer__c = uqa_iteration.Id;
            ua_list.add(ua_iteration);
          }
        }
      }
      insert ua_list;
    }
    return uta.Id;
  }
}
