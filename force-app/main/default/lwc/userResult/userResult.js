/**
 * Created by mikhailsapozhnikov on 2/12/21.
 */

import { LightningElement, api, wire, track } from "lwc";
import getResult from "@salesforce/apex/EnwayQuizController.getResult";

export default class UserResult extends LightningElement {
  @api userTestId;
  @track results;
  testName;
  userResult;
  questionAmount;
  wiredResultResult;
  userCorrectQuestionAmount;

  @wire(getResult, { userTestAnswerId: "$userTestId" })
  wiredResult(result) {
    this.wiredResultResult = result;
    if (result.data) {
      this.results = JSON.parse(result.data);
      this.setValues();
      this.error = undefined;
    } else if (result.error) {
      console.log("refresh error");
      this.error = result.error;
      console.log(this.error);
      this.results = undefined;
    }
  }

  setValues() {
    this.testName = this.results[0].testName;
    this.userCorrectQuestionAmount = this.results[0].correctQuestionAmount;
    if (this.results[0].questionAmount > 65) {
      this.userResult = Math.round(this.results[0].correctQuestionAmount / 65 * 100);
      this.questionAmount = 65;
    } else {
      this.userResult = this.results[0].result;
      this.questionAmount = this.results[0].questionAmount;
    }
  }

  onCancelClick() {
    const selectedEvent = new CustomEvent("showuserresultcomponent", {
      detail: {
        showComponent: false,
        userTestAnswerId: ""
      }
    });
    this.dispatchEvent(selectedEvent);
  }
}
