/**
 * Created by mikhailsapozhnikov on 1/29/21.
 */

import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getAllTests from "@salesforce/apex/EnwayQuizController.getAllTests";
import getRelatedResults from "@salesforce/apex/EnwayQuizController.getRelatedResult";
import { refreshApex } from "@salesforce/apex";

const resultColumns = [
  { label: "Name", fieldName: "Name", sortable: true },
  { label: "Result, %", fieldName: "Result", sortable: true, initialWidth: 110 }
];

export default class UserQuiz extends LightningElement {
  @api userTestAnswerId;
  @track selectedTestId;
  @track questionAmount;
  @track testTitle;
  @track userName;
  disableStartTestButton = true;
  showStartTestComponent = false;
  showUserTestResult = false;
  showDetails = false;
  resultColumns = resultColumns;
  defaultSortDirection = 'asc';
  sortDirection = 'asc';
  sortedBy = 'Result';

  @track tests;
  @track results;
  wiredTestResult;
  wiredResultResult;

  @wire(getRelatedResults, { testId: "$selectedTestId" })
  wiredResult(result) {
    this.wiredResultResult = result;
    if (result.data) {
      const cloneData = [...JSON.parse(result.data)];
      cloneData.sort(this.sortBy('Result', -1));
      this.results = cloneData;
      this.error = undefined;
    } else if (result.error) {
      console.log("refresh error");
      this.error = result.error;
      this.results = undefined;
    }
  }

  get userResultLength() {
    return this.results ? this.results.length : false;
  }

  @wire(getAllTests)
  wiredTest(result) {
    this.wiredTestResult = result;
    if (result.data) {
      this.tests = result.data;
      this.error = undefined;
    } else if (result.error) {
      console.log("refresh error");
      this.error = result.error;
      this.tests = undefined;
    }
  }

  handleChangeName(event) {
    this.userName = event.target.value;
    this.disableStartTestButton = this.userName.length <= 0;
  }

  handleClearCards(event) {
    this.template.querySelectorAll("c-tile").forEach((tile) => {
      tile.selected = event.target === tile;
    });
  }

  handleTileClick(event) {
    this.selectedTestId = event.detail.id;
    this.questionAmount = event.detail.questionAmount;
    this.testTitle = event.detail.name;
    this.showDetails = true;
  }

  handleShowResult(event) {
    this.userTestAnswerId = event.detail.userTestAnswerId;
    this.showUserTestResult = event.detail.showComponent;
    refreshApex(this.wiredResultResult).then(() => {
      return refreshApex(this.wiredResultResult);
    });
  }

  handleEndTest(event) {
    this.showStartTestComponent = event.detail.showComponent;
  }

  handleStartTestClick() {
    if (this.questionAmount > 0) {
      this.showStartTestComponent = true;
    } else {
      this.dispatchEvent(
          new ShowToastEvent({
            title: "Test start error",
            message: "Error: Test has no available questions!",
            variant: "error"
          })
      );
    }
  }

  sortBy(field, reverse, primer) {
    const key = primer
        ? function(x) {
          return primer(x[field]);
        }
        : function(x) {
          return x[field];
        };

    return function(a, b) {
      a = key(a);
      b = key(b);
      return reverse * ((a > b) - (b > a));
    };
  }

  onHandleSort(event) {
    const { fieldName: sortedBy, sortDirection } = event.detail;
    const cloneData = [...this.results];

    cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
    this.results = cloneData;
    this.sortDirection = sortDirection;
    this.sortedBy = sortedBy;
  }
}
