/**
 * Created by mikhailsapozhnikov on 2/4/21.
 */

import { LightningElement, api } from "lwc";

export default class Tile extends LightningElement {
  @api test;
  @api selected;

  get cardStyle() {
    return this.selected ? "slds-card slds-card_active" : "slds-card";
  }

  tileClick(evt) {
    const event = new CustomEvent("tileclick", {
      detail: {
        id: this.test.Id,
        name: this.test.Name,
        questionAmount: this.test.Question_amount__c
      }
    });
    this.dispatchEvent(event);
    this.dispatchEvent(new CustomEvent("clearcards"));
  }

}
