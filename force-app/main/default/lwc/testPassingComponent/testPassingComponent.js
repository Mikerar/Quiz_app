/**
 * Created by mikhailsapozhnikov on 2/5/21.
 */

import { LightningElement, api, track, wire } from "lwc";
import {
  getRecordCreateDefaults
} from "lightning/uiRecordApi";
import USER_QUESTION_ANSWER_OBJECT from "@salesforce/schema/User_Question_Answer__c";
import getRelatedAnswers from "@salesforce/apex/EnwayQuizController.getRelatedAnswers";
import getRelatedQuestions from "@salesforce/apex/EnwayQuizController.getRelatedQuestion";
import saveUserTestMethod from "@salesforce/apex/EnwayQuizController.saveUserTest";


export default class TestPassingComponent extends LightningElement {
  answers;
  questions;
  questionLength;
  userTestAnswer = {};
  userAnswers = new Map();
  userQuestionAnswers = [];
  wiredAnswerResult;
  questionNumber = 1;
  singleAnswer = true;
  wiredQuestionResult;
  disablePrevButton = true;
  disableNextButton = false;
  progressStep;
  @track progress;
  @track question;
  @track questionId;
  @track selectedValue = [];
  @track options = [];
  @track userQuestionArray = [];
  @api userName;
  @api testName;
  @api testId;

  // Getting Answer Type Picklist values using wire service
  @wire(getRelatedAnswers, { questionId: "$questionId" })
  typePicklistValues(result) {
    this.wiredAnswerResult = result;
    if (result.data) {
      this.answers = result.data;
      this.error = undefined;
      this.checkCorrectAnswerCount(this.answers);
      let optionsValues = [];
      for (let i = 0; i < result.data.length; i++) {
        optionsValues.push({
          label: result.data[i].Answer_Long__c,
          value: result.data[i].Id
        });
      }
      this.options = optionsValues;
    } else if (result.error) {
      this.error = result.error;
      this.answers = undefined;
    }
  }

  @wire(getRelatedQuestions, { testId: "$testId" })
  wiredQuestion(result) {
    this.wiredQuestionResult = result;
    if (result.data) {
      this.questions = result.data;
      if (this.questions.length > 65) {
        let arrayForSort = [...this.questions];
        this.questions = this.shuffle(arrayForSort);
        this.questions = this.questions.slice(0, 65);
      } else {
        let arrayForSort = [...this.questions];
        this.questions = this.shuffle(arrayForSort);
      }
      this.questionId = this.questions[0].Id;
      this.question = this.questions[0].Question_Text__c;
      this.setQuestionLength(this.questions);
      this.setProgress();
      this.createUserTestAnswer();
      this.createUserQuestionAnswer();
      this.error = undefined;
    } else if (result.error) {
      console.log("refresh error");
      this.error = result.error;
      this.questions = undefined;
    }
  }

  @wire(getRecordCreateDefaults, { objectApiName: USER_QUESTION_ANSWER_OBJECT })
  userQuestionAnswerCreateDefaults;

  setProgress() {
    this.progressStep = 100 / this.questionLength;
    this.progress = this.progressStep;
  }

  shuffle(arr){
    for(let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random()*(i + 1));
      const temp = arr[j];
      arr[j] = arr[i];
      arr[i] = temp;
    }
    return arr;
  }
  loadSelectedValue() {
    this.selectedValue = [];
    if (
        this.userAnswers[this.questionId] !== undefined &&
        this.userAnswers[this.questionId].length !== 0
    ) {
      if (this.userAnswers[this.questionId].length < 2) {
        this.selectedValue = this.userAnswers[this.questionId][0].Answer__c;
      } else {
        for (let i = 0; i < this.userAnswers[this.questionId].length; i++) {
          this.selectedValue.push(
              this.userAnswers[this.questionId][i].Answer__c
          );
        }
      }
    }
  }

  createUserAnswer() {
    if (typeof this.selectedValue === "string") {
      this.selectedValue = { 0: this.selectedValue };
    }
    let arr = [];
    for (let i = 0; i < Object.entries(this.selectedValue).length; i++) {
      arr.push({
        User_Question_Answer__c: this.questionId,
        Answer__c: this.selectedValue[i]
      });
    }
    this.userAnswers[this.questionId] = arr;
  }

  createUserQuestionAnswer() {
    for (let i = 0; i < this.questions.length; i++) {
      this.userQuestionAnswers.push({
        Question__c: this.questions[i].Id
      });
    }
  }

  createUserTestAnswer() {
    this.userTestAnswer = {
      Test__c: this.testId,
      User_Name__c: this.userName
    };
  }

  checkCorrectAnswerCount(answers) {
    let correctAnswerCount = 0;
    for (let i = 0; i < answers.length; i++) {
      if (answers[i].isCorrect__c) {
        correctAnswerCount++;
      }
    }
    this.singleAnswer = correctAnswerCount <= 1;
  }

  showUserResult(userTestAnswerId) {
    const selectedEvent = new CustomEvent("showuserresultcomponent", {
      detail: {
        showComponent: true,
        userTestAnswerId: userTestAnswerId
      }
    });
    this.dispatchEvent(selectedEvent);
  }

  setQuestionLength(questions) {
    this.questionLength = questions.length;
    if (questions.length === 1) {
      this.disableNextButton = true;
    }
  }

  handleSaveAnswers() {
    this.createUserAnswer();
    saveUserTestMethod({
      userTestAnswer: JSON.stringify(this.userTestAnswer),
      userQuestionAnswers: JSON.stringify(this.userQuestionAnswers),
      userAnswers: JSON.stringify(this.userAnswers)
    })
        .then((result) => {
          this.handleCancelClick();
          this.showUserResult(result);
        })
        .catch((error) => {
          console.log("Error ", JSON.parse(JSON.stringify(error)));
        });
  }

  handleChange(event) {
    this.selectedValue = event.detail.value;
  }

  handleCancelClick() {
    this.isStartTestOpen = false;
    const selectedEvent = new CustomEvent("starttestcomponent", {
      detail: {
        showComponent: this.isStartTestOpen,
        testId: ""
      }
    });
    this.dispatchEvent(selectedEvent);
  }

  handlePreviousPage() {
    this.createUserAnswer();
    this.disablePrevButton = this.questionNumber === 2;
    this.disableNextButton = false;
    this.questionNumber = this.questionNumber - 1;
    this.questionId = this.questions[this.questionNumber - 1].Id;
    this.question = this.questions[this.questionNumber - 1].Question_Text__c;
    this.loadSelectedValue();
    this.progress -= this.progressStep;
  }

  handleNextPage() {
    this.createUserAnswer();
    this.disableNextButton = this.questionNumber === this.questionLength - 1;
    this.disablePrevButton = false;
    this.questionNumber = this.questionNumber + 1;
    this.questionId = this.questions[this.questionNumber - 1].Id;
    this.question = this.questions[this.questionNumber - 1].Question_Text__c;
    this.loadSelectedValue();
    this.progress += this.progressStep;
  }
}
